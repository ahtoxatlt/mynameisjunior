<?php

$entries = array();
$entries[0]["displayname"] = "Ivan";
$entries[0]["age"] = 32;
$entries[1]["displayname"] = "Anna";
$entries[1]["age"] = 24;
$entries[2]["displayname"] = "Bob";
$entries[2]["age"] = 29;
$entries[3]["displayname"] = "Stuart";
$entries[3]["age"] = 28;

For ($i = 0; $i < count($entries); $i++) {
    usort($entries,function($a,$b) {
        return $a['age'] <=> $b['age'];
    });
    echo $entries[$i]["displayname"] . ' ' . $entries[$i]["age"] . '<br>';
}
//echo '<pre>';
//print_r($entries);
//echo '</pre>';