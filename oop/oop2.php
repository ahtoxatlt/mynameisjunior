<?php
include 'GrandParents.php';
include 'MyParents.php';
include 'MyChildren.php';

$grandParent = new GrandParents();
$grandParent->name = 'Marina';
$grandParent->firstName = 'Lyalina';
$grandParent->age = 56;
$grandParent->grandchildren = 3;

$parent = new MyParents();
$parent->name = 'Anton';
$parent->firstName='Lyalin';
$parent->age = 35;
$parent->money = 10000;

$children = new MyChildren();
$children->name = 'Kristina';
$children->firstName = 'Lyalina';
$children->age = 10;
$children->toys = "barbie";
$children->milkTooth = true;

$result = $parent->getName();
$result2 = $children->getToys();
echo "$result" . ' ' . "$result2";