<?php
class UserController
{
    public $patronymic;

    public $name;

    public $surname;

    public $loadData;

    public function __construct($data)
    {
        $this->loadData = $data;
    }

    public function getFio()
    {
        return $this->surname . ' ' . $this->name . ' ' . $this->patronymic;
    }

    public function load()
    {
        if ($this->loadData) {
            foreach ($this->loadData as $key => $item) {
                $this->{$key} = $item;
            }
        } else {
            echo 'bad request';
        }
    }
}