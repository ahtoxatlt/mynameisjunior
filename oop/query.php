<?php


abstract class query
{
    abstract function insert();
    abstract function select();
    abstract function update();
    abstract function clear();
}